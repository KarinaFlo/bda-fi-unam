whenever sqlerror exit rollback;
Prompt conectando como usuario karina0104
connect karina0104/karina

declare
	v_count number;
begin
  select count(*) into v_count
  from user_tables
  where table_name='T04_MY_SCHEMA';
  if v_count > 0 then
    execute immediate 'drop table t04_my_schema';
  end if;
end;
/
Prompt creando tabla t04_my_schema
create table t04_my_schema(
  username varchar2(128),
  schema_name varchar2(128)
);

Prompt otorgando privilegios para insertar en la tabla
Prompt Para el usuario karina0104 cuando autentique como sysdba => esquema:sys
grant insert on karina0104.t04_my_schema to sys;
Prompt Para el usuario karina0105 cuando autentique como sysoper => esquema:public
grant insert on t04_my_schema to public;
Prompt Para el usuario karina0106 cuando autentique como sysbackup => esquema:sysbackup
grant insert on t04_my_schema to sysbackup;

Prompt Insertando en t04_my_schema con karina0104 as sysdba
connect karina0104/karina as sysdba
insert into karina0104.t04_my_schema(username,schema_name)
values(
  sys_context('USERENV','CURRENT_USER'),
  sys_context('USERENV','CURRENT_SCHEMA')
);

Prompt Insertando en t04_my_schema con karina0105 as sysoper
connect karina0105/karina as sysoper
insert into karina0104.t04_my_schema(username,schema_name)
values(
  sys_context('USERENV','CURRENT_USER'),
  sys_context('USERENV','CURRENT_SCHEMA')
);

Prompt Insertando en t04_my_schema con karina0106 as sysbackup
connect karina0106/karina as sysbackup
insert into karina0104.t04_my_schema(username,schema_name)
values(
  sys_context('USERENV','CURRENT_USER'),
  sys_context('USERENV','CURRENT_SCHEMA')
);
commit;

Prompt Conectando como sys
connect sys/hola1234# as sysdba

Prompt Mostrando datos del archivo de passwords en v$pwfile_users
set linesize window
col username format a20
select username,sysdba,sysoper,sysbackup,last_login
from v$pwfile_users;

Prompt Cambiando el password de sys
alter user sys identified by system1;

whenever sqlerror continue none
--disconnect
