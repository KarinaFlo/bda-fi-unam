whenever sqlerror exit rollback;
Prompt conectando como usuario sys
connect sys/hola1234# as sysdba
set serveroutput on
Prompt validando la existencia de usuarios

declare
  v_count number;
  cursor cur_usuarios is
    select username
    from all_users
    where username in ('KARINA0105','KARINA0106');
begin
  for r in  cur_usuarios loop
    dbms_output.put_line('Eliminando '||r.username);
    execute immediate 'drop user '||r.username||' cascade';
  end loop;
end;
/

Prompt creando usuario KARINA0105
create user karina0105 identified by karina;
grant create session to karina0105;

Prompt create usuario KARINA0106
create user karina0106 identified by karina;
grant create session to karina0106;

Prompt asignando privilegios de administracion
grant sysdba to karina0104;
grant sysoper to karina0105;
grant sysbackup to karina0106;

whenever sqlerror continue none
disconnect
