--!export ORACLE_SID=kfgbda1
Prompt Proporcione el password de sys
connect sys as sysdba

Prompt Conectando como sys
whenever sqlerror exit rollback;

set serveroutput on

declare
  v_count number;
begin
  select count(*) into v_count
  from all_tables
  where table_name ='T02_DB_ROLES'
  and owner = 'KARINA0104';

  if v_count > 0 then
    --eliminar la tabla
    execute immediate 'drop table karina0104.t02_db_roles';
  end if;
end;
/

Prompt creando tabla karina0104.t02_db_roles

create table karina0104.t02_db_roles(
  role_id number,
  role varchar2(128)
);

Prompt insertando datos
insert into karina0104.t02_db_roles(role_id,role)
  select role_id,role
  from dba_roles;

Prompt codigo para tabla de privilegos

declare
  v_count number;
begin
  select count(*) into v_count
  from all_tables
  where table_name ='T03_DBA_PRIVS'
  and owner = 'KARINA0104';

  if v_count > 0 then
    --eliminar la tabla
    execute immediate 'drop table karina0104.t03_dba_privs';
  end if;
end;
/

Prompt creando tabla karina0104.t03_dba_privs

create table karina0104.t03_dba_privs(
  privilege varchar2(128)
);

Prompt insertando datos
insert into karina0104.t03_dba_privs(privilege)
  select privilege
  from dba_sys_privs;

whenever sqlerror continue none;
--disconnect