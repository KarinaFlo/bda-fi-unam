#!/bin/bash
#@Autor Karina Flores Garcia
#@Fecha 23/09/2020
#@Descripcion Script que automatiza 
#Descarga de imagenes con shell scripting
archivoImg="${1}"
numImagenes="${2}"
archivoZip="${3}"

#
#Funcion ayuda
#Parametros:
#parametro 1: valor del status
#

function ayuda(){
  status="${1}"
  cat s-02-ayuda.sh
  exit "${status}"
}

#Validando el primer parametro : nombre del archivo con la lista de imagenes
if [ -z "${archivoImg}" ]; then
#-z indica si la cadena esta vacia
  echo "ERROR: El nombre del archivo de imagenes es requerido"
  ayuda 100
else
  if ! [ -f "${archivoImg}" ]; then
  #-f indica si el archivo es valido
    echo "ERROR: El archivo ${archivoImg} no existe"
    ayuda 101
  fi
fi

#Validando parametro 2: numero de imagenes
#rango valido: [1,90]
if [[ "${numImagenes}" =~ [0-9]+ && "${numImagenes}" -gt 0 
  && "${numImagenes}" -le 90 ]]; then
  #-gt mayor o igual
  #-le menor o igual
  #-qe igual 
  totalImagenes=${numImagenes}
else
	echo "ERROR: El numero de imagenes es incorrecto"
	ayuda 102
fi

#Validar parametro 3
if [ -n "${archivoZip}" ]; then
#-n si el archivo  no es nulo
  archivoSalida="${archivoZip}"
  #dirname extrae el nombre de un directorio apartir de una cadena
  dirSalida=$(dirname "${archivoZip}")
  nombreZip=$(basename "${archivoZip}")
  if [ -d "${archivoSalida}" ];then
  	if [ "${nombreZip}" = "imagenes"];then
  		nombreZip= "${nombreZip}-$(date '+%Y-%m-%d-%H-%M-%S').zip"
  	else
  		echo "Error: La carpeta no es la correcta"
  		ayuda 103
  	fi
  else
  	echo "ERROR: La ruta indicada no existe"
  	ayuda 103
  fi
else
  #TMP apunta a TMP para archivos temporales
  #USER usuario actual
  dirSalida="${TMP}"/"${USER}"/imagenes
  mkdir -p "${dirSalida}"
  nombreZip="imagenes-$(date '+%Y-%m-%d-%H-%M-%S').zip"
fi

#Parametros validos
#Limpieza, eliminar posibles descargas anteriores
sudo rm -rf "${dirSalida}"/*

#Leer el archivo que contiene la lista de imagenes
count=1
while read -r linea
do
  if [ "${count}" -le "${totalImagenes}" ]; then
    #wget para descargar la imagen
    sudo wget -q -P "${dirSalida}" "${linea}"
    #-q que no muestre toda la salida
    #-P directorio donde se guarda la  imagen
    status=$?
    if [ "${status}" -eq 0 ]; then
      count=$((count+1))
    else
  	  echo "ERROR: Error en la descarga"
  	  ayuda 104
    fi
  fi
done < "${archivoImg}"
# < lee el archivoImg

#Generando el archivo zip
#IMG_ZIP_FILE es una variable de entorno
export IMG_ZIP_FILE="${dirSalida}"/"${nombreZip}"
#Para crear un archivo ZIP
zip "${IMG_ZIP_FILE}" "${dirSalida}"/*
chmod 700 ${IMG_ZIP_FILE}
#TODO generar el archivo con la lista de imagenes descargadas
#Usar basename, cut, awk 
#PARA EJECUTAR source ./ejercicio2.sh


ls "${dirSalida}" | grep jpg > ./s-00-lista-archivos.txt