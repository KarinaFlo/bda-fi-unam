--@Autor:  Flores García Karina
--@Fecha creación:  12/12/2020
--@Descripción: CONFIGURACIÓN MANUAL SHARED MEMORY MANAGEMENT

whenever sqlerror exit rollback
connect sys/system2 as sysdba
set serveroutput on

dbms_output.put_line("Configuracion manual shared memory management");


alter system set memory_target = 0M scope=memory;
alter system set sga_target= 0M scope=memory;
alter system set pga_aggregate_target = 752M scope=memory;
alter system set db_cache_size = 132M scope=memory;
alter system set shared_pool_size = 232M scope=memory;
alter system set large_pool_size = 4M scope=memory;
alter system set java_pool_size = 4M scope=memory;


exec dbms_session.sleep(5);

insert into karina0404.t02_memory_param_values values(
    3,
	(SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL),
	(select trunc(value/1048576,2) from v$parameter where name='memory_target'),
	(select trunc(value/1048576,2) from v$parameter where name='sga_target'),
	(select TRUNC(value/(1024*1024),2) from v$parameter where name='pga_aggregate_target'),
	(select TRUNC(value/(1024*1024),2) from v$parameter where name='shared_pool_size'),
	(select TRUNC(value/(1024*1024),2) from v$parameter where name='large_pool_size'),
	(select TRUNC(value/(1024*1024),2) from v$parameter where name='java_pool_size'),
	(select TRUNC(value/(1024*1024),2) from v$parameter where name='db_cache_size')
);  
commit;


whenever sqlerror continue