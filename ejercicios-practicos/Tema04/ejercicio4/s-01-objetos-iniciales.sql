--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de procedimiento, tablas e inserciones ejercicio4 Tema4

whenever sqlerror exit rollback
connect sys/system2 as sysdba
set serveroutput on

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0404';
  v_table1 varchar2(30) := 'T01_MEMORY_AREAS';
  v_table2 varchar2(30) := 'T02_MEMORY_PARAM_VALUES';
  v_table3 varchar2(30) := 'T03_MEMORY_PARAM_INFO';

begin
  --Verificar si la tabla T01 existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table1
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;

  --Verificar si la tabla T02 existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table2
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table2;
  end if;

  --Verificar si la tabla T03 existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table3
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table3;
  end if;
end;
/

--Tabla T01_MEMORY_AREAS
create table karina0404.t01_memory_areas(
  id number(4,0),
  sample_date varchar2(19),
  redo_buffer_size number(7,2),
  buffer_cache_size number(7,2),
  shared_pool_size number(7,2),
  large_pool_size number(5,0),
  java_pool_size number(2,0),
  sga_size number(6,2),
  sga_free_memory number(5,0),
  max_pga_allocated number(6,2),
  aggregate_pga_target_param number(4)
);

insert into karina0404.t01_memory_areas values(
  1,
  (select TO_CHAR(sysdate, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL),
  (select trunc(bytes/(1024*1024),2) from v$sgainfo where name='Redo Buffers'),
  (select trunc(bytes/(1024*1024),2) from v$sgainfo where name='Buffer Cache Size'),
  (select trunc(bytes/(1024*1024),2) from v$sgainfo where name='Shared Pool Size'),
  (select trunc(bytes/(1024*1024),2) from v$sgainfo where name='Large Pool Size'),
  (select trunc(bytes/(1024*1024),2) from v$sgainfo where name='Java Pool Size'),
  (select trunc(sum(value)/(1024*1024),2) from v$sga),
  (select trunc(current_size/(1024*1024),2) from v$sga_dynamic_free_memory),
  (select trunc(value/(1024*1024),2) from v$pgastat where name='maximum PGA allocated'),
  (select trunc(value/(1024*1024),2) from v$pgastat where name='aggregate PGA target parameter')
);

--Tabla T02_MEMORY_PARAM_VALUES
create table karina0404.t02_memory_param_values(
  id number(4,0),
  sample_date varchar2(19),
  memory_target number(7,0),
  sga_target number(7,0),
  pga_aggregate_target number(7,0),
  shared_pool_size number(7,0),
  large_pool_size number(7,0),
  java_pool_size number(7,0),
  db_cache_size number(7,0)
);

insert into karina0404.t02_memory_param_values values(
  1,
  (SELECT TO_CHAR(sysdate, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL),
  (select trunc(value/1048576,2) from v$parameter where name='memory_target'),
  (select trunc(value/1048576,2) from v$parameter where name='sga_target'),
  (select trunc(value/(1024*1024),2) from v$parameter where name='pga_aggregate_target'),
  (select trunc(value/(1024*1024),2) from v$parameter where name='shared_pool_size'),
  (select trunc(value/(1024*1024),2) from v$parameter where name='large_pool_size'),
  (select trunc(value/(1024*1024),2) from v$parameter where name='java_pool_size'),
  (select trunc(value/(1024*1024),2) from v$parameter where name='db_cache_size')
);

--Tabla T03_MEMORY_PARAM_INFO
create table karina0404.t03_memory_param_info(
  num number(4),
  name varchar2(18),
  value number(12),
  default_value number(12,0),
  isdefault varchar2(5),
  isses_modifiable varchar2(5),
  issys_modifiable varchar2(10)
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='memory_target'),
  (select name from v$parameter where name='memory_target'),
  (select value from v$parameter where name='memory_target'),
  (select default_value from v$parameter where name='memory_target'),
  (select isdefault from v$parameter where name='memory_target'),
  (select isses_modifiable from v$parameter where name='memory_target'),
  (select issys_modifiable from v$parameter where name='memory_target')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='memory_max_target'),
  (select name from v$parameter where name='memory_max_target'),
  (select value from v$parameter where name='memory_max_target'),
  (select default_value from v$parameter where name='memory_max_target'),
  (select isdefault from v$parameter where name='memory_max_target'),
  (select isses_modifiable from v$parameter where name='memory_max_target'),
  (select issys_modifiable from v$parameter where name='memory_max_target')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='sga_target'),
  (select name from v$parameter where name='sga_target'),
  (select value from v$parameter where name='sga_target'),
  (select default_value from v$parameter where name='sga_target'),
  (select isdefault from v$parameter where name='sga_target'),
  (select isses_modifiable from v$parameter where name='sga_target'),
  (select issys_modifiable from v$parameter where name='sga_target')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='sga_max_size'),
  (select name from v$parameter where name='sga_max_size'),
  (select value from v$parameter where name='sga_max_size'),
  (select default_value from v$parameter where name='sga_max_size'),
  (select isdefault from v$parameter where name='sga_max_size'),
  (select isses_modifiable from v$parameter where name='sga_max_size'),
  (select issys_modifiable from v$parameter where name='sga_max_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='shared_pool_size'),
  (select name from v$parameter where name='shared_pool_size'),
  (select value from v$parameter where name='shared_pool_size'),
  (select default_value from v$parameter where name='shared_pool_size'),
  (select isdefault from v$parameter where name='shared_pool_size'),
  (select isses_modifiable from v$parameter where name='shared_pool_size'),
  (select issys_modifiable from v$parameter where name='shared_pool_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='large_pool_size'),
  (select name from v$parameter where name='large_pool_size'),
  (select value from v$parameter where name='large_pool_size'),
  (select default_value from v$parameter where name='large_pool_size'),
  (select isdefault from v$parameter where name='large_pool_size'),
  (select isses_modifiable from v$parameter where name='large_pool_size'),
  (select issys_modifiable from v$parameter where name='large_pool_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='java_pool_size'),
  (select name from v$parameter where name='java_pool_size'),
  (select value from v$parameter where name='java_pool_size'),
  (select default_value from v$parameter where name='java_pool_size'),
  (select isdefault from v$parameter where name='java_pool_size'),
  (select isses_modifiable from v$parameter where name='java_pool_size'),
  (select issys_modifiable from v$parameter where name='java_pool_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='db_cache_size'),
  (select name from v$parameter where name='db_cache_size'),
  (select value from v$parameter where name='db_cache_size'),
  (select default_value from v$parameter where name='db_cache_size'),
  (select isdefault from v$parameter where name='db_cache_size'),
  (select isses_modifiable from v$parameter where name='db_cache_size'),
  (select issys_modifiable from v$parameter where name='db_cache_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='streams_pool_size'),
  (select name from v$parameter where name='streams_pool_size'),
  (select value from v$parameter where name='streams_pool_size'),
  (select default_value from v$parameter where name='streams_pool_size'),
  (select isdefault from v$parameter where name='streams_pool_size'),
  (select isses_modifiable from v$parameter where name='streams_pool_size'),
  (select issys_modifiable from v$parameter where name='streams_pool_size')
);

insert into karina0404.t03_memory_param_info values(
  (select num from v$parameter where name='log_buffer'),
  (select name from v$parameter where name='log_buffer'),
  (select value from v$parameter where name='log_buffer'),
  (select default_value from v$parameter where name='log_buffer'),
  (select isdefault from v$parameter where name='log_buffer'),
  (select isses_modifiable from v$parameter where name='log_buffer'),
  (select issys_modifiable from v$parameter where name='log_buffer')
);

--Procedimiento
create or replace procedure karina0404.spv_consulta_random_data is
  --declaracion del cursor
  cursor cur_t01_random_data is
  select id,r_varchar as caracter,r_char,r_integer,r_double,
  r_date,r_timestamp
  from karina0402.t01_random_data
  order by id;  
  --declaracion de variables
  v_username varchar2(30):= 'KARINA0404';
  v_caracter char(1):='@';
  v_elemento char(1);
  v_total_registros number(5):= 0;
  v_total_ocurrencias number(10) := 0;
  v_id number(18);
  v_r_varchar varchar2(1024);
  v_r_char char(18);
  v_r_integer number(10);
  v_r_double number(20,10);
  v_r_date date;
  v_r_timestamp timestamp(6);


begin
  open cur_t01_random_data;
  loop
    fetch cur_t01_random_data
    into v_id,v_r_varchar,v_r_char,v_r_integer,
    v_r_double,v_r_date,v_r_timestamp;
    exit when cur_t01_random_data%notfound;
    for i in 1..length(v_r_varchar)+1 loop
      v_elemento:=substr(v_r_varchar,i,1);
      if v_elemento = v_caracter then
        v_total_ocurrencias := v_total_ocurrencias + 1;
      end if;
    end loop;
    v_total_registros := v_total_registros + 1;
  end loop;
  dbms_output.put_line(
    'Registros totales para ('||v_caracter||'):'
    ||v_total_ocurrencias||' matches');
  dbms_output.put_line('Total registros: '||cur_t01_random_data%rowcount);
  close cur_t01_random_data;
end;
/
--show errors
whenever sqlerror continue