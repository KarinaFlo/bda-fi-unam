--@Autor: Flores García Karina
--@Fecha creación:  11/11/2020
--@Descripción: Creacion de tablas e inserciones

whenever sqlerror exit rollback
set serveroutput on

connect sys as sysdba
declare
  v_count number;
  v_username varchar2(30) := 'KARINA0401';
begin
  --Verificar si la tabla 1 existe
  select count(*) into v_count
  from all_tables
  where table_name= 'T01_SGA_COMPONENTS'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T01_SGA_COMPONENTS';
  end if;

  --Verificar si la tabla 2 existe
  select count(*) into v_count
  from all_tables
  where table_name= 'T02_SGA_DYNAMIC_COMPONENTS'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T02_SGA_DYNAMIC_COMPONENTS';
  end if;

  --Verificar si la tabla 3 existe
  select count(*) into v_count
  from all_tables
  where table_name= 'T03_SGA_MAX_DYNAMIC_COMPONENT'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T03_SGA_MAX_DYNAMIC_COMPONENT';
  end if;

  --Verificar si la tabla 4 existe
  select count(*) into v_count
  from all_tables
  where table_name= 'T04_SGA_MIN_DYNAMIC_COMPONENT'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T04_SGA_MIN_DYNAMIC_COMPONENT';
  end if;

  --Verificar si la tabla 5 existe
  select count(*) into v_count
  from all_tables
  where table_name= 'T05_SGA_MEMORY_INFO'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T05_SGA_MEMORY_INFO';
  end if;

  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name='T06_SGA_RESIZEABLE_COMPONENTS'
  and owner = v_username;

  if v_count > 0 then
  execute immediate 'drop table '|| v_username ||'.'||'T06_SGA_RESIZEABLE_COMPONENTS';
  end if;

  commit;
end;  
/


--Creando tabla 1
create table karina0401.t01_sga_components(
  memory_target_param  number(20,2),
  fixed_size number(20,2),
  variable_size number(20,2),
  database_buffers number(20,2),
  redo_buffers number(20,2),
  total_sga number(20,2)
);

--Insertando en tabla 1
insert into karina0401.t01_sga_components values(
  (select TRUNC(value/(1024*1024),2) from v$spparameter where name='memory_target'),
  (select TRUNC(value/(1024*1024),2) from v$sga where name='Fixed Size'),
  (select TRUNC(value/(1024*1024),2) from v$sga where name='Variable Size'),
  (select TRUNC(value/(1024*1024),2) from v$sga where name='Database Buffers'),
  (select TRUNC(value/(1024*1024),2) from v$sga where name='Redo Buffers'),
  (select TRUNC((SUM(value)/(1024*1024)),2) from v$sga where value is not null)
);

--Creando tabla 2
create table karina0401.t02_sga_dynamic_components(
  component_name varchar2(64),
  current_size_mb number(20,2),
  operation_count number(20,0),
  last_operation_type varchar2(13),
  last_operation_time date
);

--Insertando en tabla 2
insert into karina0401.t02_sga_dynamic_components(
  component_name,
  current_size_mb,
  operation_count,
  last_operation_type,
  last_operation_time
  )
  select component,(TRUNC(current_size*(1024/1024),2)),oper_count,last_oper_type,last_oper_time 
  from v$sga_dynamic_components;

--Creando tabla 3
create table karina0401.t03_sga_max_dynamic_component(
  component_name varchar2(64),
  current_size_mb number(20,2) 
);

--Insertando en tabla 3
insert into karina0401.t03_sga_max_dynamic_component(
  component_name,
  current_size_mb
  )
  select component,TRUNC(current_size*(1024/1024),2) from v$sga_dynamic_components where
    current_size=(select MAX(current_size) from  v$sga_dynamic_components);

--Creando tabla 4
create table karina0401.t04_sga_min_dynamic_component(
  component_name varchar2(64),
  current_size_mb number(20,2) 
);

--Insertando en tabla 4
insert into karina0401.t04_sga_min_dynamic_component(
  component_name,
  current_size_mb
  )
  select component,TRUNC(current_size*(1024/1024),2) from v$sga_dynamic_components where
    current_size=(select MIN(current_size)from v$sga_dynamic_components
      where current_size>0);

--Creando tabla 5
create table karina0401.t05_sga_memory_info(
  name varchar2(64),
  current_size_mb number(20,2) 
);

--Insertando en la tabla 5
insert into karina0401.t05_sga_memory_info(name,current_size_mb)
  select name, TRUNC(bytes/(1024*1024),2) from v$sgainfo where name='Maximum SGA Size';
insert into karina0401.t05_sga_memory_info(name,current_size_mb)
  select name, TRUNC(bytes/(1024*1024),2) from v$sgainfo where name='Free SGA Memory Available';

--Creando tabla 6
create table karina0401.t06_sga_resizeable_components(
  name varchar2(64)
); 

--Insertando en la tabla 6
insert into karina0401.t06_sga_resizeable_components(
  name) select name from v$sgainfo where resizeable='Yes';