--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla T04_PGA_PROCESS e insercion ejercicio3 Tema4

whenever sqlerror exit rollback
set serveroutput on
connect sys/system2 as sysdba

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0403';
  v_table1 varchar2(30) := 'T04_PGA_PROCESS';
begin
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table1
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;
end;
/

create table karina0403.t04_pga_process(
	PROCESS_TYPE varchar2(15),
	TOTAL_PROCESS number(3),
	TOTAL_PGA_ALLOCATED_MB number(10,2),
	TOTAL_PGA_USED_MB number(10,2) 
);

insert all into karina0403.t04_pga_process values(
	'bkg-process',
	(select count(*) from v$process where pname is not null),
	(select TRUNC(sum(PGA_ALLOC_MEM)/(1024*1024),2) from v$process where BACKGROUND is not null),
	(select TRUNC(sum(PGA_USED_MEM)/(1024*1024),2) from v$process where BACKGROUND is not null)
) into karina0403.t04_pga_process values(
	'other-process',
	(select count(*) from v$process where pname is null),
	(select TRUNC(sum(PGA_ALLOC_MEM)/(1024*1024),2) from v$process where BACKGROUND is null),
	(select TRUNC(sum(PGA_USED_MEM)/(1024*1024),2) from v$process where BACKGROUND is null)
) select * from DUAL; 

