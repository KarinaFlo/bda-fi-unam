--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla T02_SHARED_POOL e insercion ejercicio3 Tema4

whenever sqlerror exit rollback
set serveroutput on
connect sys/system2 as sysdba

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0403';
  v_table1 varchar2(30) := 'T02_SHARED_POOL';
begin
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table1
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;
end;
/

create table karina0403.t02_shared_pool(
    shared_pool_param_mb number(4),
    shared_pool_sga_info_mb number(4),
    resizeable varchar2(3),
    shared_pool_component_total number(4),
    shared_pool_free_memory number(13,8)
);


insert into karina0403.t02_shared_pool values(
	(select value from v$parameter where name='shared_pool_size'),
	(select TRUNC(bytes/(1024*1024),8) from v$sgainfo where name='Shared Pool Size'),
	(select resizeable from v$sgainfo where name='Shared Pool Size'),
	(select count(pool) from v$sgastat where pool='shared pool'),
	(select TRUNC(bytes/(1024*1024),8) from v$sgastat where name='free memory' and pool = 'shared pool')
);