--@Autor:  Flores García Karina
--@Fecha creación:  03/01/2021
--@Descripción: Creacion de usuario 
connect sys as sysdba

whenever sqlerror exit rollback
set serveroutput on

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0403';
begin
  --Verificar si existe el usuario
  select count(*) into v_count 
  from all_users
  where username=v_username;
  --Si no existe
  if v_count=0 then
    -- Creando usuario
    execute immediate 'create user' 
      || v_username 
      || ' identified by karina quota unlimited on users';
    execute immediate 'grant create table, create session to' || v_username;
  --Si existe el usuario
  else
    dbms_output.put_line('El usuario '|| v_username||' ya existe');
  end if;
end;  
/
whenever sqlerror continue