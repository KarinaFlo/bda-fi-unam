--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla T01_REDO_LOG_BUFFER e insercion ejercicio3 Tema4

whenever sqlerror exit rollback
set serveroutput on
connect sys/system2 as sysdba

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0403';
  v_table1 varchar2(30) := 'T01_REDO_LOG_BUFFER';
begin
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table1
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;
end;
/

create table karina0403.t01_redo_log_buffer(
    redo_buffer_size_param_mb number(9,8),
    redo_buffer_sga_info_mb number(9,8),
    resizeable varchar2(3)
);

insert into karina0403.t01_redo_log_buffer values(
	(select TRUNC(value/(1024*1024),8) from v$parameter where name='log_buffer'),
	(select TRUNC(value/(1024*1024),8) from v$sga where name='Redo Buffers'),
	(select resizeable from v$sgainfo where name='Redo Buffers')
);


