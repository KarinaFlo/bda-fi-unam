--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla T03_PGA_STATS e insercion ejercicio3 Tema4

whenever sqlerror exit rollback
set serveroutput on
connect sys/system2 as sysdba

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0403';
  v_table1 varchar2(30) := 'T03_PGA_STATS';
begin
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table1
  and owner = v_username;
  --Si existe la tabla, entonces se borra
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;
end;
/

create table karina0403.t03_pga_stats(
    max_pga_mb number(12,5),
    pga_target_parameter_mb number(4),
    pga_total_actual_mb number(12,5),
    pga_in_use_actual_mb number(12,5),
    pga_free_memory_mb number(12,5),
    pga_process_count number(3),
    pga_in_use_workareas_auto number(3)
);

insert into karina0403.t03_pga_stats values(
    (select value/(1024*1024) value_mb from v$pgastat where name='maximum PGA allocated'),
    (select value/(1024*1024) value_mb from v$pgastat where name='aggregate PGA target parameter'),
    (select value/(1024*1024) value_mb from v$pgastat where name='total PGA allocated'),
    (select value/(1024*1024) value_mb from v$pgastat where name='total PGA inuse'),
    (select value/(1024*1024) value_mb from v$pgastat where name='total freeable PGA memory'),
    (select value from v$pgastat where name='process count'),
    (select value from v$pgastat where name='total PGA used for auto workareas')    
);

