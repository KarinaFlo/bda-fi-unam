--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla t04

--Crear tabla t04_db_buffer_sysstats
create table karina0402.t04_db_buffer_sysstats(
  db_blocks_gets_from_cache number,
  consistent_gets_from_cache number,
  physical_reads_cache number,
  cache_hit_radio number generated always as 
  (trunc(1-(physical_reads_cache/(db_blocks_gets_from_cache 
    + consistent_gets_from_cache)),6))
);

--Insertar a la tabla
insert into karina0402.t04_db_buffer_sysstats(
  db_blocks_gets_from_cache,
  consistent_gets_from_cache,
  physical_reads_cache
) values (
  (select value as db_blocks_gets_from_cache from v$sysstat where name ='db block gets from cache'),
  (select value as consistent_gets_from_cache from v$sysstat where name ='consistent gets from cache'),
  (select value as physical_reads_cache from v$sysstat where name ='physical reads cache')
);

--Consultar a la tabla
select * from karina0402.t04_db_buffer_sysstats;