--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de respaldo de pfile

connect sys/system2 as sysdba

whenever sqlerror exit rollback
set serveroutput on
create pfile='/u04/db_flash_cache/initkfgbda2.ora' from spfile;   
alter system set db_flash_cache_file = '/u04/db_flash_cache/kfgbda2_flash.cache' scope=spfile;
alter system set db_flash_cache_size=50M scope=spfile;

--startup pfile='/u01/app/oracle/product/18.0.0/dbhome_1/dbs/initkfgbda2.ora'
