--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tabla t03

--Crear tabla t03_db_buffer_cache
create table karina0402.t03_db_buffer_cache(
    block_size number,
    current_size number,
    buffers number,
    target_buffers number,
    prev_size number,
    prev_buffers number,
    default_pool_size number
);

--Insertar a la tabla
insert into karina0402.t03_db_buffer_cache(
  block_size, 
  current_size,
  buffers,
  target_buffers,
  prev_size, 
  prev_buffers,
  default_pool_size) values(
    (select block_size from v$buffer_pool),
    (select current_size from v$buffer_pool),
    (select buffers from v$buffer_pool),
    (select target_buffers from v$buffer_pool),
    (select prev_size from v$buffer_pool),
    (select prev_buffers from v$buffer_pool),
    (select value from v$parameter where name='db_cache_size')
  );

--Consulta a la tabla
select * from karina0402.t03_db_buffer_cache;