--@Autor:  Flores García Karina
--@Fecha creación:  03/12/2020
--@Descripción: Creacion de tablas e inserciones sript 2 Tema4
whenever sqlerror exit rollback
set serveroutput on
connect sys/system2 as sysdba


declare
v_count number;
v_username varchar2(30) := 'KARINA0402';
v_table1 varchar2(30) := 'T01_RANDOM_DATA';
v_table2 varchar2(30) := 'T02_DB_CACHE_STATS';
v_sequence varchar2(30) := 'SEQ_T01_RANDOM_DATA';
begin
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name=v_table1
  and owner = v_username;
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table1;
  end if;
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name = v_table2
  and owner = v_username;
  if v_count > 0 then
    execute immediate 'drop table '|| v_username ||'.'||v_table2;
  end if;
  --Verificar si existe la secuencia
  select count(*) into v_count
  from all_sequences
  where sequence_name = v_sequence
  and sequence_owner = v_username;
  if v_count > 0 then
    execute immediate 'drop sequence '||v_username||'.'||v_sequence;
  end if; 
  commit;
end;  
/

--Crear secuencia
create sequence karina0402.seq_t01_random_data;

create table karina0402.t01_random_data(
    id number(18,0),
    r_varchar varchar2(1024),
    r_char char(18),
    r_integer number(10,0),
    r_double number(20,10),
    r_date date,
    r_timestamp timestamp
);

create table karina0402.t02_db_cache_stats(
  sample_date varchar2(20),
  db_blocks_gets_from_cache number,
  consistent_gets_from_cache number,
  physical_reads_cache number,
  cache_hit_radio number generated always as 
  (trunc(1-(physical_reads_cache/(db_blocks_gets_from_cache 
    + consistent_gets_from_cache)),6))
);

/*
insert into karina0402.t01_random_data(
    id, 
    r_varchar, 
    r_char, 
    r_integer, 
    r_double, 
    r_date, 
    r_timestamp)
    values (
        seq_t01_random_data.nextval,
        sys.dbms_random.string('P', 1024),
        sys.dbms_random.string('U', 18),
        trunc(sys.dbms_random.value(1,9999999999)),
        trunc(sys.dbms_random.value(1,9999999999)),
        to_date(trunc(sys.dbms_random.value(1721424,5373484)), 'J') + sys.dbms_random.normal,
        current_timestamp);
*/

--select sum(length(r_varchar)) from karina0402.t01_random_data where id = sys.dbms_random.value(?,?)

/*
insert into karina0402.t02_db_cache_stats(
    sample_date,
    db_blocks_gets_from_cache,
    consistent_gets_from_cache,
    physical_reads_cache,

) values ((SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL),
(select value as db_blocks_gets_from_cache from v$sysstat where name ='db block gets from cache'),
(select value as consistent_gets_from_cache from v$sysstat where name ='consistent gets from cache'),
(select value as physical_reads_cache from v$sysstat where name ='physical reads cache')
);
*/