--@Autor: Flores García Karina
--@Fecha creacion: 21/10/2020
--@Descripcion: Creación del diccionario de datos
connect sys/system2 as sysdba
Prompt Conectando como sys
set serveroutput on

whenever sqlerror exit rollback;

Prompt Ejecucion de scripts como usuario sys
@?/rdbms/admin/catalog.sql
@?/rdbms/admin/catproc.sql
@?/rdbms/admin/utlrp.sql

connect system/system2 
Prompt Conectando como system

Prompt Ejecucion de scripts como usuario system
@?/sqlplus/admin/pupbld.sql