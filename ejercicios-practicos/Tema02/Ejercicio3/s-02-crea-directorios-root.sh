#@Autor: Flores García Karina
#@Fecha creacion: 21/10/2020
#@Descripcion: Creación de carpetas y su configuracion para la 
#instrucción create database
#!/bin/bash
echo "Creando directorios para la instruccion create database"
mkdir -p /u01/app/oracle/oradata/KFGBDA2
mkdir -p /u02/app/oracle/oradata/KFGBDA2
mkdir -p /u03/app/oracle/oradata/KFGBDA2

echo "Cambiando de dueño y grupo a /u02 y /u03"
chown root:oinstall /u02
chown root:oinstall /u03

echo "Cambiando de dueño y usuario a oracle"
chown -R oracle:oinstall /u01/app
chown -R oracle:oinstall /u02/app
chown -R oracle:oinstall /u03/app

echo "Cambiando permisos a los directorios"
chmod -R 754 /u01/app/oracle/oradata/${ORACLE_SID^^}
chmod -R 754 /u02
chmod -R 754 /u03