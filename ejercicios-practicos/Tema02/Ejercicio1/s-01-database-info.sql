--@Autor:  Flores García Karina
--@Fecha creación:  13/10/2020
--@Descripción:  Extracción de parametros de una instancia
whenever sqlerror exit rollback
set serveroutput on

declare
  v_count number;
  v_username varchar2(30) := 'KARINA0201';
begin
  --Verificar si existe el usuario
  select count(*) into v_count 
  from all_users
  where username = v_username;
  --Si no existe
  if v_count = 0 then
    -- Creando usuario
    execute immediate 'create user ' 
      || v_username 
      || ' identified by karina quota unlimited on users';
    execute immediate 'grant create table, create session to ' || v_username;
  --Si existe el usuario
  else
    dbms_output.put_line('El usuario '|| v_username||' ya existe');
  end if;
  --Verificar si la table existe
  select count(*) into v_count
  from all_tables
  where table_name='DATABASE_INFO'
  and owner = v_username;

  --Si la tabla existe, eliminarla
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.DATABASE_INFO';
  end if;

  --Crear la tabla
  execute immediate '
    create table '||v_username||'.database_info(
      instance_name varchar2(16),
      db_domain varchar2(20),
      db_charset varchar2(15),
      sys_timestamp varchar2(40),
      timezome_offset varchar2(10),
      db_block_size_bytes number(5,0),
      os_block_size_bytes number(5,0),
      redo_block_size_bytes number(5,0),
      total_components number(5,0),
      total_components_mb number(10,2),
      max_component_name varchar2(30),
      max_component_desc varchar2(64),
      max_component_mb number(10,0)
      )';
  --Insertando datos a la tabla
  execute immediate 'insert into '||v_username||'.database_info (
    instance_name,
    db_domain,
    db_charset,
    sys_timestamp,
    timezome_offset,
    db_block_size_bytes,
    os_block_size_bytes,
    redo_block_size_bytes,
    total_components,
    total_components_mb,
    max_component_name,
    max_component_desc,
    max_component_mb
    ) values ( 
    (select sys_context(''USERENV'',''INSTANCE_NAME'') as instance from dual),
    (select sys_context(''USERENV'',''DB_DOMAIN'') from dual),
    (select value$ from sys.props$ where name=''NLS_CHARACTERSET''),
    (select systimestamp from dual),
    (select tz_offset(''America/Mexico_City'') from dual),
    (select value from v$parameter where name=''db_block_size''),
    1024,
    (select distinct blocksize from v$log),
    (select count(*) from v$sysaux_occupants),
    (select sum(space_usage_kbytes)/1024 from v$sysaux_occupants),
    (select occupant_name from v$sysaux_occupants where 
    space_usage_kbytes=(select max(space_usage_kbytes) from v$sysaux_occupants)),
    (select occupant_desc from v$sysaux_occupants where 
    space_usage_kbytes=(select max(space_usage_kbytes) from v$sysaux_occupants)),
    (select space_usage_kbytes/1024 from v$sysaux_occupants where 
    space_usage_kbytes=(select max(space_usage_kbytes) from v$sysaux_occupants))
    )';

    commit;
 end;
 /
col MAX_COMPONENT_DESC format a15

Prompt mostrando datos parte 1
set linesize window

select instance_name,db_domain,db_charset,sys_timestamp,timezome_offset
from karina0201.database_info;

Prompt mostrando datos parte 2

select db_block_size_bytes,os_block_size_bytes,redo_block_size_bytes,
total_components,total_components_mb
from karina0201.database_info;

Prompt mostrando datos parte 3;

select max_component_name,max_component_desc,max_component_mb
from karina0201.database_info;

whenever sqlerror continue none