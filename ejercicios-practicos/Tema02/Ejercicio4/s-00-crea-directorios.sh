#@Autor: Flores García Karina
#@Fecha creacion: 27/10/2020
#@Descripcion: Creación de carpeta
#!/bin/bash
echo "Creando directorio"
mkdir -p /unam-bda/ejercicios-practicos/t0204
echo "Cambiando de grupo a oinstall"
chown kgarcia:oinstall /unam-bda/ejercicios-practicos/t0204
echo "Cambiando permisos a 744"
chmod 774 /unam-bda/ejercicios-practicos/t0204