connect sys as sysdba

whenever sqlerror exit rollback
set serveroutput on


alter session set nls_date_format = 'dd/mm/yyyy hh24:mi:ss';
alter system set db_writer_processes=2 scope=spfile;
--alter system set db_writer_processes=1 scope=spfile;
alter system set log_buffer = 7776256 scope=spfile;
--alter system set log_buffer = 7766016 scope=spfile;
alter system set db_files = 250 scope=spfile;
--alter system set db_files = 200 scope=spfile;
alter system set dml_locks = 2500 scope=spfile;
--alter system set dml_locks = 2480 scope=spfile;
alter system set transactions = 600 scope=spfile;
--alter system set transactions = 620 scope=spfile;
alter session set hash_area_size = 136192;
alter system set hash_area_size = 136192 scope=spfile;
--  alter system set hash_area_size = 131072 scope=spfile;  
alter session set sort_area_size = 70656;
--alter session set sort_area_size = 65536;
alter system set sql_trace = true scope=memory;
alter system set optimizer_mode = FIRST_ROWS_100 scope=both;
alter session set cursor_invalidation = deferred;

declare
  v_count number;
  v_username varchar2(20) := 'KARINA0204';

begin
  -- Comprobando la existencia de la tabla T03_UPDATE_PARAM_SESSION
  select count(*) into v_count
  from all_tables
  where table_name = 'T03_UPDATE_PARAM_SESSION'
  and owner = v_username;

  -- Eliminando tabla en caso de existir
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T03_UPDATE_PARAM_SESSION';
  end if;

  -- Comprobando la existencia de la tabla T04_UPDATE_PARAM_INSTANCE
  select count(*) into v_count
  from all_tables
  where table_name = 'T04_UPDATE_PARAM_INSTANCE'
  and owner = v_username;

  -- Eliminando tabla en caso de existir
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T04_UPDATE_PARAM_INSTANCE';
  end if;

  -- Comprobando la existencia de la tabla
  select count(*) into v_count
  from all_tables
  where table_name = 'T05_UPDATE_PARAM_SPFILE'
  and owner = v_username;

  -- Eliminando tabla en caso de existir T05_UPDATE_PARAM_SPFILE
  if v_count > 0 then
    execute immediate 'drop table '||v_username||'.T05_UPDATE_PARAM_SPFILE';
  end if;  

end;
/

--Creacion de la tabla t03_update_param_session con sus valores
--Parametros modificados en la sesion
create table karina0204.t03_update_param_session as
    select name,value
    from v$parameter
    where name in (
        'cursor_invalidation','optimizer_mode',
        'sql_trace','sort_area_size','hash_area_size','nls_date_format',
        'db_writer_processes','db_files','dml_locks','log_buffer','transactions'
    )
    and value is not null; 

--Creacion de la tabla t04_update_param_instance con sus valores
--Parametros modificados en la instancia
create table karina0204.t04_update_param_instance as
  select name,value
  from v$system_parameter
  where name in (
    'cursor_invalidation','optimizer_mode',
    'sql_trace','sort_area_size','hash_area_size','nls_date_format',
    'db_writer_processes','db_files','dml_locks','log_buffer','transactions'
  )
  and value is not null;

--Creacion de la tabla t05_update_param_spfile con sus valores
--parametros modificados en el spfile
create table karina0204.t05_update_param_spfile as
  select name,value
  from v$spparameter
  where name in (
    'cursor_invalidation','optimizer_mode',
    'sql_trace','sort_area_size','hash_area_size','nls_date_format',
    'db_writer_processes','db_files','dml_locks','log_buffer','transactions'
  )
  and value is not null;
  
--Creación del pfile
create 
pfile='/unam-bda/ejercicios-practicos/t0204/e-03-spparameter-pfile.txt' 
from spfile;    

whenever sqlerror continue





