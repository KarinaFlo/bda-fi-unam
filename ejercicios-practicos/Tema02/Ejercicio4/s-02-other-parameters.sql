whenever sqlerror exit rollback
set serveroutput on

connect sys as sysdba

declare
v_count number;
v_username varchar2(30) := 'KARINA0204';

begin
      --Verificar si la table existe
    select count(*) into v_count
        from all_tables
        where table_name = 'T02_OTHER_PARAMETERS'
        and owner = v_username;

    --Si la tabla existe, eliminarla
    if v_count > 0 then
		  execute immediate 'drop table '|| v_username ||'.T02_OTHER_PARAMETERS';
		end if;

    --Crear la tabla
    execute immediate '
      create table '||v_username||'.t02_other_parameters (
        num NUMBER,
        name varchar2(80),
        type NUMBER,
        value varchar2(4000),
        display_value varchar2(4000),
        isdefault varchar2(9),
        is_session_modifiable varchar2(5),
        is_system_modifiable varchar2(9)
        )';

end;
/
--INSERCION A LA TABLA
insert into karina0204.t02_other_parameters(
    num, name, type, value, display_value, isdefault, is_session_modifiable, 
    is_system_modifiable) select num, name, type, value, display_value, isdefault, 
    isses_modifiable, issys_modifiable from v$parameter where value is not null;

commit;


whenever sqlerror continue
