--@Autor: Flores García Karina
--@Fecha creacion: 19/10/2020
--@Descripcion: Creación del archivo spfile
Prompt Proporcione el password de sys
connect sys as sysdba

Prompt Conectando como sys
whenever sqlerror exit rollback;
set serveroutput on

startup nomount
create spfile from pfile;
Prompt Verificando el nomount
!ls $ORACLE_HOME/dbs/spfilekfgbda2.ora
/
whenever sqlerror continue none