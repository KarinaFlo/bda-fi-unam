#@Autor: Flores García Karina
#@Fecha creacion: 19/10/2020
#@Descripcion: Creación de loop devices para una base de datos
#!/bin/bash
echo "Creando directorio unam-bda"
mkdir -p /unam-bda
echo "Cambiando a unam-bda"
cd /unam-bda
echo "Creando archivos para loop devices"
dd if=/dev/zero of=disk2.img bs=100M count=10
dd if=/dev/zero of=disk3.img bs=100M count=10
echo "Comprobando la creacion de los archivos"
du -sh disk*.img
echo "Creando loop devices"
losetup -fP disk2.img
losetup -fP disk3.img
echo "Confirmando la creacion e los loop devices"
losetup -a
echo "Dandole formato a los archivos"
mkfs.ext4 disk2.img
mkfs.ext4 disk3.img
echo "Creando directorio u02"
mkdir /u02
echo "Creando directorio u03"
mkdir /u03