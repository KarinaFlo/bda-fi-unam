#@Autor: Flores García Karina
#@Fecha creacion: 19/10/2020
#@Descripcion: Creación de archivo de passwords y de parámetros
#!/bin/bash
archivoPwd="${ORACLE_HOME}"/dbs/orapwkfgbda2
echo "Haciendo export de ORACLE_SID"
export ORACLE_SID=kfgbda2
echo "Creando archivo de passwords"
orapwd FILE=${archivoPwd} FORMAT=12.2 \
    FORCE=Y \
    SYS=password 
echo "Creando archivo initkfgbda2.ora"
touch $ORACLE_HOME/dbs/initkfgbda2.ora
echo "db_name=kfgbda2" >> ${ORACLE_HOME}/dbs/initkfgbda2.ora
echo "control_files=(/u01/app/oracle/oradata/KFGBDA2/control01.ctl,
      /u02/app/oracle/oradata/KFGBDA2/control02.ctl,
      /u03/app/oracle/oradata/KFGBDA2/control03.ctl)" >> $ORACLE_HOME/dbs/initkfgbda2.ora
echo "memory_target=768M" >> $ORACLE_HOME/dbs/initkfgbda2.ora
